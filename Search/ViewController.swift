//
//  ViewController.swift
//  Search
//
//  Created by CPU11901 on 8/23/17.
//  Copyright © 2017 TriDao. All rights reserved.
//

import UIKit
import ImageSlideshow

class ViewController: UIViewController {

  override func viewDidLoad() {
    super.viewDidLoad()
    let imageSlideshow = ImageSlideshow(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
    imageSlideshow.setImageInputs([ImageSource(image: UIImage(named: "nuoc_hat_chia_d2_an_loc_phuc")!), ImageSource(image: UIImage(named: "nuoc_mat_mien_tay_an_loc_phuc")!)])
    
    self.view.addSubview(imageSlideshow)
    
    // Do any additional setup after loading the view, typically from a nib.
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }

}
