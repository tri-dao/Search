import UIKit


// MARK: *** ProductListDataManagerProtocol
class ProductListDataManager: ProductListDataManagerProtocol {
  var requester: ProductListInteractorProtocol?
  
  func retrieveProductList(){
    guard let path = Bundle.main.path(forResource: "Data", ofType: "json") else {
      if let callback = requester as? ProductListDataManagerCallbackProtocol {
        callback.onError(requester!)
      }
      return
    }
    
    let url = URL(fileURLWithPath: path)
    
    do {
      let data = try Data(contentsOf: url)
      let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
      if let dict = json as? [String: AnyObject] {
        if let callback = requester as? ProductListDataManagerCallbackProtocol {
          let products: [Product] = ProductParser.parse(data: dict) as! [Product]
          callback.onRetrieveProducts(requester!, products: products)
        }
      }
    } catch {
      if let callback = requester as? ProductListDataManagerCallbackProtocol {
        callback.onError(requester!)
      }
    }
  }
  
  func retrieveProductListDirectly() -> [Product] {
    guard let path = Bundle.main.path(forResource: "Data", ofType: "json") else {
      return []
    }
    
    let url = URL(fileURLWithPath: path)
    
    do {
      let data = try Data(contentsOf: url)
      let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
      if let dict = json as? [String: AnyObject] {
        let products: [Product] = ProductParser.parse(data: dict) as! [Product]
        return products
      }
    } catch { }
    return []
  }
}
