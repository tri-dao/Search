import UIKit

class ProductParser: ProductListDataManagerParserProtocol {
  class func parse(data: [String : AnyObject]) -> [AnyObject] {
    var productList: [Product] = [Product]()
    guard let products = data["products"] as? [[String: AnyObject]] else { return productList }
    
    for product in products {
      let productDetail = Product(
        id: UInt64("\(product["id"]!)")!,
        name: product["name"] as! String,
        provider: product["provider"] as! String,
        manu: product["manu"] as! String,
        isAvailable: (UInt32("\(product["isAvailable"]!)")! != 0)
      )
      productDetail.numOfLike = UInt32("\(product["numOfLike"]!)")!
      productDetail.perDiscount = Double("\(product["perDiscount"]!)")!
      productDetail.realPrice = Double("\(product["realPrice"]!)")!
      productDetail.salePrice = Double("\(product["salePrice"]!)")!
      
      productDetail.comments = CommentParser.parse(data: product) as! [Comment]
      productDetail.gifts = GiftParser.parse(data: product) as! [Gift]
      productDetail.images = ImageParser.parse(data: product) as! [UIImage]
      productList.append(productDetail)
    }
    return productList
  }
}
