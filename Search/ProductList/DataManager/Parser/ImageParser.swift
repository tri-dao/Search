import UIKit

class ImageParser: ProductListDataManagerParserProtocol {
  class func parse(data: [String : AnyObject]) -> [AnyObject] {
    var imageList: [UIImage] = [UIImage]()
    guard let images = data["images"] as? [[String: AnyObject]] else { return imageList }
    
    for image in images {
      if let imageDetail = UIImage(named: image["name"] as! String) {
        imageList.append(imageDetail)
      }
    }
    
    return imageList
  }
}
