import Foundation


// MARK: *** ProductListPresenterProtocol
class ProductListPresenter: ProductListPresenterProtocol {
  var interactor: ProductListInteractorProtocol?
  var router: ProductListRouterProtocol?
  var view: ProductCollectionViewProtocol?
  
  func viewDidLoad() {
    interactor?.retrieveProducts()
  }
  
  func showProductDetailScreen(product: Product) {
    router?.presentProductDetailScreen(view: view!, product: product)
  }
}

// MARK: *** ProductListInteractorCallbackProtocol
extension ProductListPresenter: ProductListInteractorCallbackProtocol {
  func didRetrieveProducts(_ presenter: ProductListPresenterProtocol, products: [Product]) {
    presenter.view?.showProducts(products: products)
  }
}
