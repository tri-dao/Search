import UIKit


//  MARK: *** DATA MANAGER
protocol ProductListDataManagerProtocol: class {
  var requester: ProductListInteractorProtocol? {get set}
  
  // INTERACTOR -> DATA MANAGER
  func retrieveProductList()
  func retrieveProductListDirectly() -> [Product]
}

protocol ProductListDataManagerCallbackProtocol: class {
  // DATA MANAGER -> INTERACTOR
  func onRetrieveProducts(_ interactor: ProductListInteractorProtocol, products: [Product])
  func onError(_ interactor: ProductListInteractorProtocol)
}

protocol ProductListDataManagerParserProtocol: class {
  static func parse(data: [String: AnyObject]) -> [AnyObject]
}

//  MARK: *** INTERACTOR
protocol ProductListInteractorProtocol: class {
  var dataManager: ProductListDataManagerProtocol? {get set}
  var presenter: ProductListPresenterProtocol? {get set}
  
  // PRESENTER -> INTERACTOR
  func retrieveProducts()
}

protocol ProductListInteractorCallbackProtocol: class {
  // INTERACTOR -> PRESENTER
  func didRetrieveProducts(_ presenter: ProductListPresenterProtocol, products: [Product])
}

//  MARK: *** PRESENTER
protocol ProductListPresenterProtocol: class {
  var interactor: ProductListInteractorProtocol? {get set}
  var router: ProductListRouterProtocol? {get set}
  var view: ProductCollectionViewProtocol? {get set}
  
  // VIEW -> PRESENTER
  func viewDidLoad() // get products (via interactor) and show them (via view)
  func showProductDetailScreen(product: Product)
}

//  MARK: *** ROUTER
protocol ProductListRouterProtocol: class {
  static func createProductListModule() -> UIViewController
  
  //  PRESENTER -> ROUTER
  func presentProductDetailScreen(view: ProductCollectionViewProtocol, product: Product)
}

//  MARK: *** VIEW
protocol ProductCollectionViewProtocol: class {
  var presenter: ProductListPresenterProtocol? {get set}
  
  // PRESENTER -> VIEW
  func showProducts(products: [Product])
}
