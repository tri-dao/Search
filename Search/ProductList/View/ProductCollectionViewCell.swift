import UIKit


// MARK: *** UICollectionViewCell
class ProductCollectionViewCell: UICollectionViewCell {
  @IBOutlet weak var perDiscountLabel: UILabel!
  @IBOutlet weak var perDiscountView: UIView!
  @IBOutlet weak var giftImage: UIImageView!
  @IBOutlet weak var productImage: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var salePriceLabel: UILabel!
  @IBOutlet weak var realPriceLabel: UILabel!
  
  func set(forProduct product: Product) {
    layer.borderWidth = 0.5
    layer.borderColor = UIColor(red: 238/255, green: 238/255, blue: 238/255, alpha: 1).cgColor
    
    giftImage.isHidden = product.gifts.count != 0 ? false : true
    perDiscountView.isHidden = product.perDiscount != 0 ? false : true
    if perDiscountView.isHidden {
      realPriceLabel.isHidden = true
    } else {
      perDiscountLabel.text = "\(round(product.perDiscount))%"
      realPriceLabel.isHidden = false
      
      // Set strikethrough
      let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: "\(NumberHelper.addDot(forNumber: product.realPrice, withNDecs: 0)) đ")
      attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
      realPriceLabel.attributedText = attributeString
    }
    
    if product.images.count > 0 {
      productImage.image = product.images[0]
    }
    
    nameLabel.text = product.name
    
    salePriceLabel.text = "\(NumberHelper.addDot(forNumber: product.salePrice, withNDecs: 0)) đ"
  }
}
