import UIKit


// MARK: *** UIViewController
class ProductCollectionView: UIViewController {
  @IBOutlet weak var collectionView: UICollectionView!
  var presenter: ProductListPresenterProtocol?
  var productList: [Product] = []
  
  func customNavigationItem() {
    // Set search bar
    createSearchBar()
    
    // Set scan qrcode icon
    let rightCartButtonItem:UIBarButtonItem = UIBarButtonItem(image: UIImage(named: "icon_scan_qrcode"), style: UIBarButtonItemStyle.plain, target: self, action: nil)
    navigationItem.setRightBarButton(rightCartButtonItem, animated: true)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    presenter?.viewDidLoad()
    collectionView.delegate = self
    collectionView.dataSource = self
    
    let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
    layout.minimumInteritemSpacing = 0
    layout.minimumLineSpacing = 0
    collectionView.collectionViewLayout = layout
    
    customNavigationItem()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    navigationController?.navigationBar.barTintColor = UIColor(red: 33/255, green: 150/255, blue: 243/255, alpha: 1)
  }
}

// MARK: *** UISearchBarDelegate
extension ProductCollectionView: UISearchBarDelegate {
  func createSearchBar() {
    let searchBar = UISearchBar()
    searchBar.showsCancelButton = false
    searchBar.placeholder = "Sản phẩm, thương hiệu và mọi thứ bạn cần tìm"
    searchBar.delegate = self
    navigationItem.titleView = searchBar
  }
}

// MARK: *** UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
extension ProductCollectionView: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath) as! ProductCollectionViewCell
    
    let product = productList[indexPath.row]
    cell.set(forProduct: product)
    
    return cell
  }
  
  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    return CGSize(width: collectionView.bounds.width / 2, height: collectionView.bounds.height / 2)
  }
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return productList.count
  }
  
  func numberOfSections(in collectionView: UICollectionView) -> Int {
    return 1
  }
  
  func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    presenter?.showProductDetailScreen(product: productList[indexPath.row])
  }
}

// MARK: *** ProductListViewProtocol
extension ProductCollectionView: ProductCollectionViewProtocol {
  func showProducts(products: [Product]) {
    productList = products
    collectionView?.reloadData()
  }
}
