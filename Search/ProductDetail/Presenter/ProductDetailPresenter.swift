import UIKit


// MARK: *** ProductDetailPresenterProtocol
class ProductDetailPresenter: ProductDetailPresenterProtocol {
  var view: ProductDetailViewProtocol?
  var router: ProductDetailRouterProtocol?
  var product: Product!
  
  func viewDidLoad() {
    view?.showProduct(forProduct: product)
  }
}
