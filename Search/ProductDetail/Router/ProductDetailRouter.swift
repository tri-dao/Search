import UIKit


// MARK: *** ProductDetailRouterProtocol
class ProductDetailRouter: ProductDetailRouterProtocol {
  class func createProductDetailModule(forProduct product: Product) -> UIViewController {
    let viewController = mainStoryboard.instantiateViewController(withIdentifier: "ProductDetailController")
    if let view = viewController as? ProductDetailView {
      let presenter: ProductDetailPresenterProtocol = ProductDetailPresenter()
      let router: ProductDetailRouterProtocol = ProductDetailRouter()
      
      view.presenter = presenter
      presenter.view = view
      presenter.router = router
      presenter.product = product
      
      return viewController
    }
    
    return UIViewController()
  }
  
  static var mainStoryboard: UIStoryboard {
    return UIStoryboard(name: "Main", bundle: Bundle.main)
  }
}
