import Foundation

class Comment {
  let id: UInt64
  var content: String
  
  init(id: UInt64, content: String) {
    self.id = id
    self.content = content
  }
}
