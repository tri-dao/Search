import XCTest
@testable import Search

class SearchTests: XCTestCase {
  var dataManager: ProductListDataManager?
  
  override func setUp() {
    super.setUp()
    dataManager = ProductListDataManager()
  }
  
  override func tearDown() {
    super.tearDown()
  }
  
  func testProduct(desired: Product, real: Product) {
    XCTAssert(desired.id == real.id)
    XCTAssert(desired.manu.compare(real.manu).rawValue == 0)
    XCTAssert(desired.name.compare(real.name).rawValue == 0)
    XCTAssert(desired.numOfLike == real.numOfLike)
    XCTAssert(desired.perDiscount == real.perDiscount)
    XCTAssert(desired.provider.compare(real.provider).rawValue == 0)
    XCTAssert(desired.realPrice == real.realPrice)
    XCTAssert(desired.salePrice == real.salePrice)
    XCTAssert(desired.isAvailable == real.isAvailable)
    
    // Compare comments
    if desired.comments.count == real.comments.count {
      XCTAssert(true)
      for iComment in 0..<desired.comments.count {
        XCTAssert(desired.comments[iComment].id == real.comments[iComment].id)
        XCTAssert(desired.comments[iComment].content.compare(real.comments[iComment].content).rawValue == 0)
      }
    } else {
      XCTAssert(false)
    }
    
    // Compare gifts
    if desired.gifts.count == real.gifts.count {
      XCTAssert(true)
      for iGift in 0..<desired.gifts.count {
        XCTAssert(desired.gifts[iGift].id == real.gifts[iGift].id)
        XCTAssert(desired.gifts[iGift].manu.compare(real.gifts[iGift].manu).rawValue == 0)
        XCTAssert(desired.gifts[iGift].name.compare(real.gifts[iGift].name).rawValue == 0)
        XCTAssert(desired.gifts[iGift].price == real.gifts[iGift].price)
      }
    } else {
      XCTAssert(false)
    }
  }
  
  func testGetDataFromJSONFile() {
    if let products = dataManager?.retrieveProductListDirectly() {
      // The number of products
      XCTAssert(products.count == 3)
      
      // Product 1
      var desiredProduct = Product(
        id: 123,
        name: "Nước Mát Miền Tây Ân Lộc Phúc (240ml)",
        provider: "Ân Lộc Phúc",
        manu: "Ân Lộc Phúc",
        isAvailable: true
      )
      desiredProduct.numOfLike = 2
      desiredProduct.perDiscount = 50
      desiredProduct.realPrice = 19000
      desiredProduct.salePrice = 9500
      desiredProduct.gifts = [Gift(id: 456, name: "Nước Hạt Chia D2 Ân Lộc Phúc (240ml)", manu: "Ân Lộc Phúc", price: 15000)]
      testProduct(desired: desiredProduct, real: products[0])
      
      // Product 2
      desiredProduct = Product(
        id: 456,
        name: "Nước Hạt Chia D2 Ân Lộc Phúc (240ml)",
        provider: "Ân Lộc Phúc",
        manu: "Ân Lộc Phúc",
        isAvailable: true
      )
      desiredProduct.numOfLike = 10
      desiredProduct.perDiscount = 10
      desiredProduct.realPrice = 15000
      desiredProduct.salePrice = 13500
      testProduct(desired: desiredProduct, real: products[1])
      
      // Product 3
      desiredProduct = Product(
        id: 789,
        name: "Nước Hạt Chia D1 Ân Lộc Phúc (240ml)",
        provider: "Ân Lộc Phúc",
        manu: "Ân Lộc Phúc",
        isAvailable: false
      )
      desiredProduct.numOfLike = 50
      desiredProduct.perDiscount = 20
      desiredProduct.realPrice = 15000
      desiredProduct.salePrice = 12000
      testProduct(desired: desiredProduct, real: products[2])
    } else {
      XCTAssert(false)
    }
  }
  
  func testPerformanceExample() {
      // This is an example of a performance test case.
      self.measure {
        // Put the code you want to measure the time of here.
    }
  }
    
}
